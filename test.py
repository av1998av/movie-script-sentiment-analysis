import numpy as np 
import pandas as pd 

from sklearn.feature_extraction.text import CountVectorizer
from numpy import loadtxt
from keras.models import load_model
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers import Dense, Embedding, LSTM, SpatialDropout1D
from sklearn.model_selection import train_test_split
from keras.utils.np_utils import to_categorical

import re
import os
import csv 
import sys 



data = pd.read_csv('./Sentiment.csv')
data = data[['text','sentiment']]

data = data[data.sentiment != "Neutral"]
data['text'] = data['text'].apply(lambda x: x.lower())
data['text'] = data['text'].apply((lambda x: re.sub('[^a-zA-z0-9\s]','',x)))

# print(data[ data['sentiment'] == 'Positive'].size)
# print(data[ data['sentiment'] == 'Negative'].size)

for idx,row in data.iterrows():
    row[0] = row[0].replace('rt',' ')
    
max_fatures = 2000
tokenizer = Tokenizer(num_words=max_fatures, split=' ')
tokenizer.fit_on_texts(data['text'].values)
X = tokenizer.texts_to_sequences(data['text'].values)
X = pad_sequences(X)
Y = pd.get_dummies(data['sentiment']).values
X_train, X_test, Y_train, Y_test = train_test_split(X,Y, test_size = 0.33, random_state = 42)
batch_size = 32

model = load_model('model.h5')

score,acc = model.evaluate(X_test, Y_test, verbose = 2, batch_size = batch_size)

l = []
f = open(sys.argv[1], "rU")
for line in f:
    for word in line.split():
        l.append(word)
i = 0
rating = 0
while i < len(l):
    twt = [' '.join(l[i:i+32])]
    twt = tokenizer.texts_to_sequences(twt)
    twt = pad_sequences(twt, maxlen=32, dtype='int32', value=0)
    sentiment = model.predict(twt,batch_size=1,verbose = 2)[0]
    rating = (rating + sentiment[0])/2
    i = i + 32
print(rating)

