pip install -r req.txt (to install dependencies)


Sentiment.csv - twitter tweet sentiment data set (40000 approximately in number)
qualified.zip - folder of movie scripts
model.h5 - trained model file
main.py -  to find sentiment for all the scripts under 'quailifed' folder
save.py - to save the trained model
test.py - to find the sentiment of a film (input - script file in .txt format as a command line argument, output - negativity percaentage)

model and scripts at..
https://drive.google.com/drive/folders/1CBycwpsx7SSzeny0fY4AUArPkr2AdLAK?usp=sharing
