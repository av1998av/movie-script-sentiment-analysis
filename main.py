#The algorithm for finding the negative sentiment percentage for a script through an LSTM (with softmax activation) is as follows

#Step1: Read data from dataset file.
#Step2: Initialise network specific variables.
#Step3: Load the model file.
#Step4: Read the scripts in a string.
#Step5: Divide them into chunks equal to the batch size.
#Step6: Tokenise the chunk and convert them to an array of strings.
#Step7: Make them into a single sting and vectorize it.
#Step8: Change the pad sequence
#Step9: Use the model to predict the sentiment
#Step10: Write it to the file

#Important variables:
#dataset size = around 36000
#batch size to feed into network while training = 32
#maximum no of features = 20000
#embeddings_dimension = 128
#lstm output dimensions = 196



import numpy as np 
import pandas as pd 

from sklearn.feature_extraction.text import CountVectorizer
from numpy import loadtxt
from keras.models import load_model
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers import Dense, Embedding, LSTM, SpatialDropout1D
from sklearn.model_selection import train_test_split
from keras.utils.np_utils import to_categorical

import re
import os
import csv 
import sys 


#data-set is being loaded
data = pd.read_csv('./Sentiment.csv') 
data = data[['text','sentiment']]

#neutral data is being removed as we are going for a binary classification (positive and negative)
data = data[data.sentiment != "Neutral"]
data['text'] = data['text'].apply(lambda x: x.lower())
data['text'] = data['text'].apply((lambda x: re.sub('[^a-zA-z0-9\s]','',x)))

#partitioning the data into postive and negative clusters
print(data[ data['sentiment'] == 'Positive'].size)
print(data[ data['sentiment'] == 'Negative'].size)


for idx,row in data.iterrows():
    row[0] = row[0].replace('rt',' ')
    
#training    
max_fatures = 2000
tokenizer = Tokenizer(num_words=max_fatures, split=' ')
tokenizer.fit_on_texts(data['text'].values)
X = tokenizer.texts_to_sequences(data['text'].values)
X = pad_sequences(X)
Y = pd.get_dummies(data['sentiment']).values
X_train, X_test, Y_train, Y_test = train_test_split(X,Y, test_size = 0.33, random_state = 42)
batch_size = 32

#loading the model file
model = load_model('model.h5')


score,acc = model.evaluate(X_test, Y_test, verbose = 2, batch_size = batch_size)
print("score: %.2f" % (score))
print("acc: %.2f" % (acc))

#path for the movie scripts folder
path = './qualified'


folder = os.fsencode(path)

#looping through all the files in the folder
for file in os.listdir(folder):
    filename = os.fsdecode(file)
    l = []
    f = open('./qualified/' + filename, "rU") #opening the file
    for line in f: #this loop is to append the words and add it to 'l' array of strings.. which is fed to the network
        for word in line.split():
            l.append(word)
    i = 0 #iterator variable to check that only 32 (batch size) words are fed into the network
    rating = 0 #variable to store the rating per batch
    while i < len(l):
        twt = [' '.join(l[i:i+32])]  #words as array elements in the array are converted into a single string
        twt = tokenizer.texts_to_sequences(twt)  #vectorizing the string to feed it into the network
        twt = pad_sequences(twt, maxlen=32, dtype='int32', value=0) #changing the pad sequences to match the 'embedding_2 input'
        sentiment = model.predict(twt,batch_size=1,verbose = 2)[0] #binary classifier step
        rating = (rating + sentiment[0])/2 #calculating the average rating for the  strings as of now and the current iteration's string
        i = i + 32
    csv_file = open('./neg_perc.csv','a')
    csv_file.write(str(filename) + ',' + str(rating)+'\n')  #writing into the output file
    print(rating)
    print(filename)

